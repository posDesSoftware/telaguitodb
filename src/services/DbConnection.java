package services;

import java.sql.Connection;
import java.sql.DriverManager;

public class DbConnection {
	Connection connection;

	public DbConnection() {
	}

	public Connection connectingDataBase() {

		try {
			// JDBC Declaration
			Class.forName("com.mysql.cj.jdbc.Driver");
			// Drive
			String dbUrl = "jdbc:mysql://localhost:3306/CADASTRO";
			// User and Password
			connection = DriverManager.getConnection(dbUrl, "root", "password");
			System.out.println("Successful connection!");
			return connection;
		} catch (Exception e) {
			System.out.println("Connection to data base failed.");
			e.printStackTrace();
			return null;
		}

	}

	public void disconnectingDataBase() {

		try {
			connection.close();
			System.out.println("Connection successfully closed.");
		} catch (Exception e) {
			System.out.println("Error. Could not close Connection.");
		}

	}

}
