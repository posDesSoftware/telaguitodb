package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import control.DAO;
import entities.Client;

public class TelaDeCadastro extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNome;
	private JTextField textFieldAddress;
	private JTextField textFieldEmail;
	private JTextField textFieldPhone;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaDeCadastro frame = new TelaDeCadastro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TelaDeCadastro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1095, 525);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Save Button action
		JButton btnSave = new JButton("SAVE");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
								
				// Calling DAO to save Client from Screen in DB
				DAO dao = new DAO();
				dao.insert(getClientFromScreen());
				reloadScreen();
			}
		});
		btnSave.setBounds(60, 126, 114, 35);
		contentPane.add(btnSave);
		
		
		// Reload Button action
		JButton reloadButton = new JButton("Reload");
		reloadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				reloadScreen();
			}
		});
		
		reloadButton.setBounds(497, 131, 114, 25);
		contentPane.add(reloadButton);
		
		// Clear Button action
		JButton clearButton = new JButton("Clear");
		clearButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clearScreen();
			}
		});
		
		clearButton.setBounds(654, 131, 114, 25);
		contentPane.add(clearButton);
		
		// Delete Button action
		JButton deleteButton = new JButton("DELETE");
		deleteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
												
				// Calling DAO to delete the client's name  row
				DAO dao = new DAO();
				dao.delete(textFieldNome.getText());
				reloadScreen();
			}
		});
		deleteButton.setBounds(892, 126, 114, 35);
		contentPane.add(deleteButton);
		
		//Update Button
		JButton updateButton = new JButton("Update");
		updateButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent actionEvent) {
				
				Client client = getClientFromScreen();
				DAO dao = new DAO();
				try {						
					dao.update(client);
					reloadScreen();	
				} catch (Exception e) {
					System.out.println("Erro on 'TelaDeCadastro' during Updating process");
					e.printStackTrace();
				}		
			}
		});
		updateButton.setBounds(324, 131, 114, 25);
		contentPane.add(updateButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 199, 1071, 286);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Name", "Address", "Fone", "Email"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, Object.class, Object.class, Object.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		scrollPane.setViewportView(table);
		
		JLabel lblNome = new JLabel("Name");
		lblNome.setFont(new Font("Dialog", Font.BOLD, 13));
		lblNome.setBounds(60, 25, 66, 15);
		contentPane.add(lblNome);
		
		JLabel lblEndereo = new JLabel("Address");
		lblEndereo.setFont(new Font("Dialog", Font.BOLD, 13));
		lblEndereo.setBounds(60, 58, 78, 17);
		contentPane.add(lblEndereo);
		
		textFieldNome = new JTextField();
		textFieldNome.setBounds(144, 23, 391, 19);
		contentPane.add(textFieldNome);
		textFieldNome.setColumns(10);
		
		textFieldAddress = new JTextField();
		textFieldAddress.setColumns(10);
		textFieldAddress.setBounds(144, 56, 391, 19);
		contentPane.add(textFieldAddress);
		
		JLabel lblFone = new JLabel("Phone");
		lblFone.setFont(new Font("Dialog", Font.BOLD, 13));
		lblFone.setBounds(596, 25, 66, 15);
		contentPane.add(lblFone);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setFont(new Font("Dialog", Font.BOLD, 13));
		lblEmail.setBounds(596, 58, 66, 15);
		contentPane.add(lblEmail);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		textFieldEmail.setBounds(672, 54, 334, 19);
		contentPane.add(textFieldEmail);
		
		textFieldPhone = new JTextField();
		textFieldPhone.setColumns(10);
		textFieldPhone.setBounds(672, 21, 334, 19);
		contentPane.add(textFieldPhone);
		
	}
	
	public void clearScreen() {
						
		textFieldNome.setText(null);
		textFieldAddress.setText(null);
		textFieldEmail.setText(null);
		textFieldPhone.setText(null);
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);			
	}
	
	public void reloadScreen() {
		clearScreen();
				
		//Calling DAO
		DAO dao = new DAO();
		try {
			// The result from Select
			ResultSet resultSet = dao.select();

			int qtdColunas = table.getModel().getColumnCount(); // count table coluns

			while (resultSet.next()) {

				String name = resultSet.getString("name");
				String address = resultSet.getString("address");
				String phone = resultSet.getString("fone");
				String email = resultSet.getString("email");

				Object[] fila = new Object[qtdColunas];
				fila[0] = name;
				fila[1] = address;
				fila[2] = phone;
				fila[3] = email;

				((DefaultTableModel) table.getModel()).addRow(fila);
				textFieldNome.requestFocus();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	//That return the client who is set on the screen
	public Client getClientFromScreen() {
		Client clientScreen = new Client();
		clientScreen.setName(textFieldNome.getText());
		clientScreen.setAddress(textFieldAddress.getText());
		clientScreen.setEmail(textFieldEmail.getText());
		clientScreen.setFone(textFieldPhone.getText());
		
		return clientScreen;
	}
}
