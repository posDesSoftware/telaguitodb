package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;

import entities.Client;
import services.DbConnection;

public class DAO {
	String sql = null;
	
	public ResultSet select() throws SQLException {
		
		// Create a connection
		DbConnection dbConnection = new DbConnection();
		// Openig the connection
		Connection connection = dbConnection.connectingDataBase();
		// Create a SQL query
		sql = "SELECT * FROM Client";
		// Executing the statment
		PreparedStatement statement = (PreparedStatement) connection.prepareStatement(sql);
		ResultSet resultSet = statement.executeQuery();
		System.out.println("Reloading GUI table...");
		return resultSet;
	}
	
	public void insert(Client client) {
		
		try {
			// Create a connection
			DbConnection dbConnection = new DbConnection();
			// Openig the connection
			Connection connection = dbConnection.connectingDataBase();
			// Create a SQL query
			sql = "INSERT INTO Client(name, address, fone, email) "
					+ "VALUES('" + client.getName() + "' " 
					+ ", '" + client.getAddress() + "' " 
					+ ", '" + client.getFone() + "' "
					+ ", '" + client.getEmail() + "')";
			// Executing the Statment
			Statement statement = connection.createStatement();
			boolean statementResult = statement.execute(sql);
			
			// Closing resources
			statement.close();
			dbConnection.disconnectingDataBase();
		} catch (Exception e) {
			System.out.println("Data insert Error!");
			System.out.println(sql);
			System.out.println(e.getStackTrace());
		}	
	}
	
	public void delete(String clientName) {
		
		try {
			// Create a connection
			DbConnection dbConnection = new DbConnection();
			// Openig the connection
			Connection connection = dbConnection.connectingDataBase();
			// Create a SQL query
			sql = "DELETE FROM Client WHERE name = " 
					+ "'" + clientName + "'";
			// Executing the Statment
			Statement statement = connection.createStatement();
			boolean statementResult = statement.execute(sql);
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println("Deleting Error!");
			System.out.println(sql);
			e.printStackTrace();
		}
	}
	
	public void update(Client client) {
		try {
			// Create a connection
			DbConnection dbConnection = new DbConnection();
			// Openig the connection
			Connection connection = dbConnection.connectingDataBase();
			// Create a SQL query
			sql = "UPDATE Client " + 
					"SET address = '" + client.getAddress() + "'," + 
					"fone = '" + client.getFone() + "'," + 
					"email = '" + client.getEmail() + "' " + 
					"WHERE name = '" + client.getName() + "'";
			// Executing the Statment
			Statement statement = connection.createStatement();
			boolean statementResult = statement.execute(sql);
			
		} catch (SQLException e) {
			// TODO: handle exception
			System.out.println("Update Error!");
			System.out.println(sql);
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
